FROM node
MAINTAINER Julian Teran
WORKDIR /APP
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start