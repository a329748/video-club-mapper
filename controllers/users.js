const express = require('express')

function list(req, res, next) {
	res.send('List of users:');
}

function index(req, res, next) {
	res.send(`Find a user by ID (${req.params.id}).`)
}

function create(req, res, next) {
	res.send(`Create a new user.`)
}

function replace(req, res, next) {
	res.send(`Replace an existing user by ID (${req.params.id}).`)
}

function update(req, res, next) {
	res.send(`Update an existing user's data by ID (${req.params.id}).`)
}

function drop(req, res, next) {
	res.send(`Delete an existing user by ID (${req.params.id}).`)
}

module.exports = {
	list, index, create, replace, update, drop
}